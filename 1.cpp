/***
FN:F79302
PID:1
GID:3
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;
double Hp_div(vector<double>& v1, vector<double>& v2)
{
	double div = 0;
	for(int i = 0; i<v1.size(); i++)
	{
	   div += v1[i]/v2[i];
	}
	return div;
}
int main ()
{
int n;
cin >> n;
vector<double> v1(0);
vector<double> v2(0);
double num;
for (int i = 0; i<n; i++)
{   
	cin >> num;
	v1.push_back (num);
}
for (int i = 0; i<n; i++)
{
	cin >> num;
	v2.push_back (num);
}
double result = Hp_div(v1,v2);
cout << result;

return 0;
}

