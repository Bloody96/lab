/***
FN:F79302
PID:3
GID:3
*/

#include <iostream>
#include <string>
#include <vector>
#include <cmath>
#include <utility>
using namespace std;
void add(vector<double>& v1)
{ 
    double n_vec;
	int n_pos;
	cin >> n_pos >> n_vec;
	v1.insert (v1.begin()+(n_pos-1),n_vec);    
}
void remove(vector<double>& v1)
{
	int n_pos;
	cin >> n_pos;
	v1.erase(v1.begin()+(n_pos-1));
}
void shift_left(vector<double>& v1)
{
	v1.push_back(v1[0]);
	v1.erase(v1.begin());
}
void reverse(vector<double>& v1)
{
	
	for (int i=0; i<=v1.size()%2;i++)
	{
		swap(v1[i],v1[v1.size()-1-i]);
	}
}
int main ()
{
int n;
cin >> n;
vector<double> v1(0);
double num;
for (int i = 0; i<n; i++)
{   
	cin >> num;
	v1.push_back (num);
}
string str;
while(cin>>str)
{
if(str == "add")
{
    add(v1); 
}
else if(str == "remove")
{
	remove(v1);
}   
else if(str == "shift_left") 
{
	shift_left(v1);
}
else if(str == "reverse") 
{
	reverse(v1);
}
else if (str == "print")
{
for (int i = 0; i<v1.size(); i++)
if(i==v1.size()-1)
cout << v1[i]<<" "<< endl;
else
cout << v1[i]<< " ";
}
if(str == "exit")
break;
}
return 0;
}
