/***
FN:F00000
PID:2
GID:1
*/

#include <iostream>

using namespace std;
double simple_future_value (double deposit, double rate, double years)
{
	double b = (deposit * rate/100)*years + deposit;
	return b; 
}
double complicated_future_value (double deposit, double rate, double years)
{
	int i;
	for (i=0;i<years;i++)
	{		
	 	double c = (deposit * rate/100);
	 	deposit = deposit + c;
    }
    	return deposit; 
}
int main()
{
	double deposit, rate, years;
	cin >> deposit >>  rate >> years;
	double balance = simple_future_value(deposit, rate, years);
	double balance2 = complicated_future_value(deposit, rate, years);
	cout << balance << endl;
	cout << balance2;
	return 0;
}
